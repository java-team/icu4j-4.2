# Copyright (C) 2006-2007, International Business Machines Corporation and
# others. All Rights Reserved.

# These files contains special code blocks for the ICU Eclipse fragment
src/com/ibm/icu/impl/ByteBuffer.java
src/com/ibm/icu/impl/ICUResourceBundleImpl.java
src/com/ibm/icu/lang/UCharacter.java
src/com/ibm/icu/text/DecimalFormat.java
src/com/ibm/icu/text/RuleBasedCollator.java
src/com/ibm/icu/util/UResourceBundle.java
